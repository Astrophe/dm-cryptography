package com.cryptography.encryption;

import com.cryptography.CryptoHelper;
import com.cryptography.N;
import com.cryptography.RSA;
import com.cryptography.Stopwatch;

import java.util.List;

public class Encoding {

    public static Encoder encoder;
    public static CryptoHelper cryptoHelper = new CryptoHelper();
    public static RSA RSA;

    public static void main(String[] args){
        String N  = (args.length >= 1) ? args[0] : "10807"; //par 1
        int E = (args.length >= 2) ? Integer.parseInt(args[1]) : 3;      //par 2
        String message = (args.length >= 3) ? args[2] :
                "The successful warrior is the average man, with laser-like focus. ~Bruce Lee"; // par 3

        Stopwatch stopwatch = new Stopwatch();

        // determining P & Q
        stopwatch.start();
        N nObj = cryptoHelper.calcPQ2(N);
        stopwatch.stop();
        Long nCalcTime = stopwatch.getDuration();

        // creating public key & encoder
        stopwatch.restart();
        RSA = new RSA(nObj.getP(), nObj.getQ(), E, false);
        encoder = new Encoder(RSA);
        stopwatch.stop();
        Long publicKeyTime = stopwatch.getDuration();

        List<Integer> encodedMessage;
        encoder = new Encoder(RSA);

        //encoding message
        stopwatch.restart();
        encodedMessage = encoder.encodeMessage(message);
        stopwatch.stop();
        Long encodingTime = stopwatch.getDuration();

        System.out.println("P is: " + RSA.getP());
        System.out.println("Q is: " + RSA.getQ());
        System.out.println("E is: " + RSA.getE());
        System.out.println("Message after encoding: " + encodedMessage);
        System.out.println("Amount of time busy calculating p & q was: " + nCalcTime + " microseconds");
        System.out.println("Amount of time busy encoding was: " + encodingTime + " microseconds");
        System.out.println("Total amount of time: " + (nCalcTime + publicKeyTime + encodingTime) + " microseconds");
        System.out.println("1 second = 1.000 milliseconds = 1.000.000 microseconds");
    }
}
