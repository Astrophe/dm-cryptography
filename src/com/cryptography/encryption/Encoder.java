package com.cryptography.encryption;

import com.cryptography.RSA;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Encoder {
    RSA RSA;

    public Encoder(RSA RSA) {
        this.RSA = RSA;
    }

    /*
        encode the message using the RSA public key, the character is converted into ascii for purpose of encoding.
        @param character is the char to be encoded
        @return the calculated int value after encoding
     */
    public int encode(char character){
        BigInteger C = BigInteger.valueOf((int)character);
        C = C.pow(RSA.getE());
        C = C.mod(BigInteger.valueOf(RSA.getN()));
        return C.intValue();
    }

    /*
        split the message into an array of characters and encode every character
        @param msg is the message to be encoded
        @return List with the encoded message
     */
    public List<Integer> encodeMessage(String msg) {
        List<Integer> result = new ArrayList<Integer>();
        char[] splitMsg = msg.toCharArray();
        for(char character : splitMsg){
            result.add(encode(character));
        }
        return result;
    }

    /*
        change public key
        @param new public key
     */
    public void setRSA(RSA pk){
        this.RSA = pk;
    }
}
