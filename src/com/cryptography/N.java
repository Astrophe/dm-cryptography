package com.cryptography;

/*
    A neat way to return multiple values
 */
public class N {

    private final int P;
    private final int Q;

    public N(int p, int q) {
        this.P = p;
        this.Q = q;
    }

    public int getP() {
        return P;
    }

    public int getQ() {
        return Q;
    }

    public int getN() {
        return P * Q;
    }
}
