package com.cryptography.decryption;

import com.cryptography.CryptoHelper;
import com.cryptography.N;
import com.cryptography.RSA;
import com.cryptography.Stopwatch;

import java.util.ArrayList;
import java.util.List;

public class Decoding {

    public static Decoder decoder;
    public static CryptoHelper cryptoHelper = new CryptoHelper();
    public static RSA RSA;

    // c:
    // 12628,7932,6411,13036,12669,5421,2770,3242,2441,11622,11234,2770,2441,11235,7200,2770,7365,3683,5421,7200,2770,8239,2441,11622,2770,7200,7932,2441,5421,3683,2770,3242,7932,2441,2770,2763,12687,11234,3683,2770,7200,7932,3683,2770,7365,3683,5421,7200,2770,2441,8239,2770,7932,2441,3242,2770,7200,7932,6411,13036,12669,5421,2770,3242,2441,11622,11234,2770,2441,11235,7200,4475,2770,8728,75,2441,7932,13036,2770,5204,2441,2441,12439,3683,13036
    // N:
    // 13493
    // e:
    // 7
    public static void main(String[] args) {
        Stopwatch stopwatch = new Stopwatch();
        List<Integer> msg = new ArrayList<>();


        int     N = Integer.valueOf(args[0]);
        int     e = Integer.valueOf(args[1]);
        String  c = args[2];

        String[] emArray = c.split(",");
        for (int i = 0; i < emArray.length; i++) msg.add(Integer.valueOf(emArray[i]));

        // Given N, calculate p and q
        stopwatch.start();
        N pq = cryptoHelper.calcPQ2(Integer.toString(N));
        stopwatch.stop();
        long timeToPQ = stopwatch.getDuration();

        //create RSA key
        RSA = new RSA(pq.getP(), pq.getQ(), e, true);

        // Decrypt given message
        decoder = new Decoder(RSA);
        stopwatch.restart();
        String m = decoder.decodeMessage(msg);
        stopwatch.stop();

        System.out.println("N is: " + RSA.getN());
        System.out.println("E is: " + RSA.getE());
        System.out.println("amount of time busy calculating p and q: " + timeToPQ + " microseconds");
        System.out.println("P is: " + RSA.getP());
        System.out.println("Q is: " + RSA.getQ());
        System.out.println("D is: " + RSA.getD());
        System.out.println("Message after decoding is: " + m);
        System.out.println("Amount of time busy decoding was: " + stopwatch.getDuration() + " microseconds");

    }
}


