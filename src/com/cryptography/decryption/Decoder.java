package com.cryptography.decryption;

import com.cryptography.RSA;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Decoder {
    RSA RSA;

    public Decoder(RSA RSA) {
        this.RSA = RSA;
    }

    // Takes an encrypted character integer value and returns the decrypted value
    public char decode(BigInteger intVal){
        return (char) (intVal
                .pow(RSA.getD())                         // Raise c to the power of d
                .mod(BigInteger.valueOf(RSA.getN())))   // Modulus N
                .intValue();
    }

    // Takes an encrypted message and iterates through each character,
    // decrypting them along the way
    public String decodeMessage(List<Integer> msg) {
        String result = "";
        for(int i = 0; i < msg.size(); i++) result += (decode(BigInteger.valueOf(msg.get(i))));
        return result;
    }
}
