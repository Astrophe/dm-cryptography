package com.cryptography;


public class Main {
    public static int N = 7927 * 104803;
    public static Stopwatch stopwatch = new Stopwatch();
    public static CryptoHelper prime = new CryptoHelper();
    public static void main(String[] args) {

//        stopwatch.start();
//        N nObj = prime.calcN(N);
//        long time1 = stopwatch.getDuration();

        stopwatch.restart();
        N nObj2 = prime.calcPQ2(Integer.toString(N));
        long time2 = stopwatch.getDuration();
        System.out.println("time 1: " + "" + ", time 2: " + time2);
        System.out.println("time 1.1: " + prime.getPrimeTime() + "time 1.2:: " + prime.getPqTime());
    }
}
