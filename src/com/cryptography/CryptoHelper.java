package com.cryptography;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CryptoHelper {
    private static final int MIN_PRIME = 2;
    private Long primeTime;
    private Long pqTime;
    Stopwatch stopwatch = new Stopwatch();

    public CryptoHelper() {
    }

    /**
        creates a list of all the integers that are a prime number, up to given int
        @param maxPrime is the maximum integer the biggest prime can be
        @return List<Integer> with all the found primes equal or smaller than maxPrime
     */
    public List<Integer> createPrimes(int maxPrime){
        stopwatch.start();
        List<Integer> primes = new ArrayList<>();
        List<Integer> primeCheck = new ArrayList<>();
        int maxPrimeCheck = (int)Math.ceil(Math.sqrt((double)maxPrime));

        for(int number = MIN_PRIME; number <= maxPrime; number++){
            if(isPrime(number, primeCheck)) {
                primes.add(number);
                if(number <= maxPrimeCheck) primeCheck.add(number);
            }
        }
        this.primeTime = stopwatch.getDuration();
        return primes;
    };

    /*
        a quick way to determine if a number is prime by checking if a number is divisible by previous primes
     */
    private boolean isPrime(int number, List<Integer> primes){
        boolean result = true;
        for(Integer prime : primes){
            boolean nonPrimeCharacteristic = number % prime == 0;
            if(nonPrimeCharacteristic){
                result = false;
                break;
            }
        }
        return result;
    }

    /**
        determine if two numbers are coprime, by calculating their greatest common divisor (gcd).
        if their gcd is 1, then they are coprime. this recursive method works with the principles of
        the euclidean algorithm.
        @param x & y are the numbers to see if they are coprime
        @return boolean whether their gcd is 1, making them coprime
     */
    public boolean isCoPrime(int x, int y){
        int z = x % y;
        if(z == 0){
            return (y == 1) ? true : false;
        } else return isCoPrime(y, z);
    }

    /**
        !!! this method is inefficient
        brute force the values p & q. Get all the primes smaller or equal to N.
        get the first prime and multiply it with the other primes. when it exceeds N
        pick the next prime and do this till you find N.
        @param publicN for which to calculate P * Q
        @return object N with found P & Q, if P & Q was is not found their value is 0
     */
    public N calcPQ2(int publicN){
        stopwatch.restart();
        N nObj = null;
        List<Integer> primes = this.createPrimes(publicN);

        boolean stop = false;
        for(int p=0; p<primes.size(); p++){
            for(int q=p; q<primes.size(); q++){
                long calcN = primes.get(p) * primes.get(q);
                if(calcN == (long)publicN) {
                    nObj = new N(primes.get(p), primes.get(q));
                    stop = true;
                } else if (calcN > publicN) break;
            }
            if(stop) break;
        }

        this.pqTime = stopwatch.getDuration();
        if(nObj != null) return nObj;
        else return nObj = new N(0,0);
    }

    public Long getPrimeTime() {
        return primeTime;
    }

    public Long getPqTime() {
        return pqTime;
    }

    /**
        this is the solution for the bottleneck off finding p & q
        Find the smaller of the two primes for N by checking if it divisible by N for
        as long as it is smaller than N/2. the second prime is calculated by N/prime1 = prime2
        @param N for which to calculate P * Q
        @return object N with found P & Q, if P & Q was is not found their value is 0
     */
    public N calcPQ2(String N){
        final BigInteger INIT_PRIME = new BigInteger(String.valueOf(MIN_PRIME));

        BigInteger n = new BigInteger(N);
        BigInteger p = INIT_PRIME;
        BigInteger q;
        // iterates through primes starting with 2;
        // if n modulus p returns zero, dividing n by p returns q
        while(p.compareTo(n.divide(INIT_PRIME)) <= 0){
            if(n.mod(p).equals(BigInteger.ZERO)){
                q = n.divide(p);
                return new N(p.intValue(), q.intValue());
            }
            p = p.nextProbablePrime();
        }
        return new N(0, 0);

    }
}
