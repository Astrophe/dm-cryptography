package com.cryptography;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class RSA {
    CryptoHelper crypto = new CryptoHelper();
    private final int P;
    private final int Q;
    private final int E;
    private final int D;

    public RSA(int p, int q, boolean calcD) {
        this.P = p;
        this.Q = q;
        this.E = (p != 0 & q != 0) ? calculateE() : 0;
        this.D = (calcD) ? calculateD(this.E, this.getPhi()) : -1;
    }

    public RSA(int p, int q, int e, boolean calcD) {
        this.P = p;
        this.Q = q;
        this.E = e;
        this.D = (calcD) ? calculateD(this.E, this.getPhi()) : -1;
    }

    //this works, however Im not confident it is the right way

    /**
     * calculate the all the possible e by getting a list that contains
     * all the prime numbers that are smaller than phi(N), remove the numbers that are not coprime with N,
     * and remove the numbers that are not coprime with phi(N)
     * @return the e that is in quarter position of the list of valid e's
     */
    private int calculateE() {
        int phi = this.getPhi();
        int n = getN();
        List<Integer> eCandidates = crypto.createPrimes(phi);

        eCandidates = removeNonCoPrime(eCandidates, n);
        eCandidates = removeNonCoPrime(eCandidates, phi);

        return eCandidates.get(eCandidates.size() / 4);
    }

    private List<Integer> removeNonCoPrime(List<Integer> list, int compare) {
        for (Iterator<Integer> iter = list.iterator(); iter.hasNext(); ) {
            int item = iter.next();
            if (!crypto.isCoPrime(compare, item)) iter.remove();
        }
        return list;
    }

    /**
     * calculate the private key using the formula d * E mod phi(N) == 1.
     * By trying every number for d until a number is found that follows the formula
     * @param E the e that s used in the formula
     * @param phi the phi that is used in the formula
     * @return
     */
    public int calculateD(int E, int phi) {
        int d = 1;
        while((d * E) % phi != 1) d++;
        return d;
    }

    public int getE() {
        return this.E;
    }

    public int getP() {
        return this.P;
    }

    public int getQ() {
        return this.Q;
    }

    public int getN() {
        return this.P * this.Q;
    }

    public int getD(){
        return this.D;
    }

    public int getPhi(){
        return (this.P - 1)*(this.Q - 1);
    }
}
